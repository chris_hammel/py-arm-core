import setuptools

#with open("README.md", "r") as fh:
#    long_description = fh.read()

setuptools.setup(
    name="py-arm-core-chris_hammel", # Replace with your own username
    version="0.0.1",
    author="Chris Hammel",
    author_email="chris.hammel@armautomation.com",
    description="Core library for ARM Automation",
    url="https://chris_hammel@bitbucket.org/chris_hammel/py-arm-core.git",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    install_requires=[
        "paho-mqtt",
        "keyboard",
        "tqdm",
        "numpy"
    ],
    python_requires='>=3.8',
)
