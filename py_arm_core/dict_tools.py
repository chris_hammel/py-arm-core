from numbers import Number
from typing import List

def merge_dicts(*args):
    '''Provide any number of dicts as arguments\n\n
    Supports dict value types: Number, List\n
    Numbers: values will be summed\n
    Lists: values will be merged
    '''
    SUPPORTED_TYPES = (Number, List)
    key_set = set()
    for d in args:
        if not isinstance(d, dict):
            raise TypeError(f'args cannot be type {type(d)}, expected {dict}')
        key_set = set([*key_set, *d])

    output = {}
    for key in key_set:
        count = 0
        new_list = []
        value_type = None
        for my_dict in [d for d in args if key in d.keys()]:
            value = my_dict[key]
            if value_type is None:
                value_type = type(value)
            else:
                if not isinstance(value, value_type):
                    raise TypeError(f'dictionary value types must be consistent: '\
                                    f'{value_type} and {type(value)} both found')
            if not issubclass(type(value), SUPPORTED_TYPES):
                raise TypeError(f'Supported types are {SUPPORTED_TYPES}: received type {type(value)}')
            if isinstance(value, Number):
                count += my_dict[key]
            elif isinstance(value, List):
                new_list = [*new_list, *value]

        if issubclass(value_type, Number):
            output.update({key: count})
        elif issubclass(value_type, List):
            output.update({key: new_list})
        else:
            raise Exception(f'value_type is not in {SUPPORTED_TYPES}, '\
                            f'but it was checked earlier...this should never happen')
    return output


if __name__=='__main__':
    a = {'one': 2, 'two': 1}
    b = {'two': 10, 'three': 3}
    c = {'two': 2, 'four': 2}

    print(a)
    print(b)
    print(c)
    print(merge_dicts(a, b, c))

    a2 = {'one': [1, 2, 3, 4, 5], 'two': [2]}
    b2 = {'two': [3], 'three': [3, 4]}
    c2 = {'two': [4, 5], 'three': [5], 'four': [4, 5]}
    e2 = {'five': []}
    d2 = {'one': 1}     #including this one should fail

    print(a2)
    print(b2)
    print(c2)
    print(merge_dicts(a2, b2, c2, e2))

    #print()
    #print()
    #print(merge_dicts(a2, b2, c2, d2))

