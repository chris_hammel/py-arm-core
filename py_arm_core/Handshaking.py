#Handshaking.py - defines handshaking between wafer devices

import sys
from enum import Enum
import logging
from typing import Callable
import time
import keyboard
try:
    from .ARM_util import *
except ImportError:
    from ARM_util import *
import tkinter
#import pdb
#from pudb import set_trace; set_trace()
# import pudb; pu.db

# fmtString = "%(asctime)s:%(msecs)010.6f [%(levelname)-6s] %(name)-12s  - %(message)s"
# logging.basicConfig(stream=sys.stdout, format=fmtString, datefmt='%H:%M:%S')

class HandshakeSide(Enum):
    Give    = 0
    Receive = 1

def other_handshake_side(current_side: HandshakeSide) -> HandshakeSide:
    if current_side is None:
        raise Exception('current_side cannot be None')
    if current_side == HandshakeSide.Give:
        return HandshakeSide.Receive
    elif current_side == HandshakeSide.Receive:
        return HandshakeSide.Give
    else:
        raise Exception(f'current_side {current_side} is not a valid HandshakeSide')

class Handshake():
    '''Handshake will be driven by the Giver, once both sides are ready
    '''
    def __init__(self,
                 name: str='',
                 require_obj_to_pass :bool = False,
                 required_obj_type=None,
                 ready_to_init_give: Callable=None,
                 ready_to_init_receive: Callable=None,
                 can_give: bool=True,
                 can_receive: bool=True,
                 name_prefix: str='',
                 log_level=logging.INFO):

        if name_prefix != '':
            name_prefix += '-'
        self.name = f'{name_prefix}-{self.__class__.__name__}-{name}'
        self.log = arm_logger(self.name, log_level=log_level)
        # self.log = logging.getLogger(self.name)
        # self.log.setLevel(log_level)

        self.can_give = can_give
        self.can_receive = can_receive

        self._my_side = None    # this will be HandshakeSide.Give or HandshakeSide.Recieve
        self._shaking_with  = None # this will be the other party in the handshake

        self._ready = False             # ready to proceed with transaction
        self._pending = False           # True if one side has initiated a handshake and is ready, but the other side has not yet said it is ready
        self._ready_to_transfer = False # True if both sides have said READY and our side is ready for begin_transfer() to be called
                                        #   if we are Giving, this will be True as soon as _pending goes False
                                        #   if we are Receiving, this will be True as soon as the other party's _transferring goes False
        self._transferring = False      # True if both sides have said READY and the transaction has begun
        self._this_side_done = False    # True if our side of transferring is complete
        self._busy = False              # True if a handshake has been initiated, False when both sides have finished _transferring

        self._require_obj_to_pass = require_obj_to_pass     # If True, will pass an object when Giving, will EXPECT to receive a passed obj when Receiving
        self._required_obj_type = required_obj_type         # optional, will enforce object type during a transaction if set

        if not ready_to_init_give:
            ready_to_init_give = self.__default_ready_to_init_give
        self.ready_to_init_give = ready_to_init_give

        if not ready_to_init_receive:
            ready_to_init_receive = self.__default_ready_to_init_receive
        self.ready_to_init_receive = ready_to_init_receive

        self._obj_to_pass = None        # object to be passed - can be left None, depending upon the above requirements

        self.__state = 0                 # current state in state machine
        self._SM = StateMachine(self.name, user_log_level=log_level)
        self.__state_history = [None]*100
        self.__prev_state = 0

    #DEFAULT READY FUNCTIONS
    def __default_ready_to_init_give(self):
        return True

    def __default_ready_to_init_receive(self):
        return True

    #GET STATUS INFO
    def print_status(self):
        print()
        print()
        print(f'name:               {self.name}')
        print(f'my_side:            {self._my_side}')
        print(f'shaking_with:       {self._shaking_with.name if self._shaking_with else None}')
        print()
        print(f'busy:               {self._busy}')
        print(f'ready:              {self._ready}')
        print(f'pending:            {self._pending}')
        print(f'ready_to_transfer:  {self._ready_to_transfer}')
        print(f'transferring:       {self._transferring}')
        print(f'this_side_done:     {self._this_side_done}')


    def get_state(self):
        return self.__state

    def is_giver(self):
        if not self._my_side:
            return False
        else:
            return self._my_side == HandshakeSide.Give

    def is_receiver(self):
        if not self._my_side:
            return False
        else:
            return self._my_side == HandshakeSide.Receive

    def is_busy(self):
        return self._busy

    def is_pending_ready(self):
        return self._pending

    def is_ready(self):
        return self._ready

    def is_ready_to_transfer(self):
        return self._ready_to_transfer

    def is_transferring(self):
        return self._transferring

    def is_done_transferring(self):
        return self._this_side_done

    def set_ready(self):
        if not self._pending:
            raise Exception('Handshake is not pending, cannot set Ready')
        self.log.debug('Ready!')
        self._ready = True

    @property
    def can_give(self):
        return self._can_give

    @can_give.setter
    def can_give(self, value: bool):
        if value is None:
            raise ValueError('value cannot be None')
        self._can_give = value

    @property
    def can_receive(self):
        return self._can_receive

    @can_receive.setter
    def can_receive(self, value: bool):
        if value is None:
            raise ValueError('value cannot be None')
        self._can_receive = value


    #HANDSHAKING ACTIONS
    def _clear_handshake_vars(self):
        self._my_side = None
        self._shaking_with = None
        self._ready = False
        self._pending = False
        self._ready_to_transfer = False
        self._transferring = False
        self._busy = False
        self._this_side_done = False
        self._obj_to_pass = None    #is this dangerous?

    def _initiate(self, other_party, shaking_side: HandshakeSide) -> bool:
        '''only call from owner of this class instance
        '''
        if not isinstance(other_party, Handshake):
            raise TypeError(f'other_party must be type {Handshake}, not {type(other_party)}')
        if self._busy:
            self.log.debug(f'PROGRAMMING BUG: self._busy is still True, \
                           this should have been checked first by \
                           other party {other_party.name}')
            return False
        if self._shaking_with:
            raise Exception(f'PROGRAMMING BUG: handshake is NOT busy, \
                            but self._shaking_with is still set!!')
        if shaking_side == HandshakeSide.Give:
            obj_to_give = self.ready_to_init_give()
            if not obj_to_give:
                self.log.debug(f'Our side not ready to init give')
                return False
            if not other_party.ready_to_init_receive():
                self.log.debug(f'Other side ({other_party.name}) not ready to init receive')
                return False
            else:
                if not self.begin_handshake(other_party,
                                            HandshakeSide.Give,
                                            obj_to_give):
                    raise Exception('Failed to begin our own handshake as Giver, LIKELY A BUG')
                if not other_party.begin_handshake(other_party=self,
                                                   shaking_side=HandshakeSide.Receive,
                                                   obj_to_pass=obj_to_give):
                    raise Exception('Other side failed to begin handshake as Receiver, LIKELY A BUG')
                return True
        elif shaking_side == HandshakeSide.Receive:
            if not self.ready_to_init_receive():
                self.log.debug('Our side not ready to init receive')
                return False
            obj_to_give = other_party.ready_to_init_give()
            if not obj_to_give:
                self.log.debug(f'Other side ({other_party.name}) not ready to init give')
                return False
            else:
                if not self.begin_handshake(other_party,
                                            HandshakeSide.Receive,
                                            obj_to_give):
                    raise Exception('Failed to begin our own handshake as Receiver, LIKELY A BUG')
                if not other_party.begin_handshake(other_party=self,
                                                   shaking_side=HandshakeSide.Give,
                                                   obj_to_pass=obj_to_give):
                    raise Exception('Other side failed to begin handshake as Giver, LIKELY A BUG')
                return True
        else:
            raise ValueError(f'shaking_side ({shaking_side}) is not a valid HandshakeSide')

    def begin_handshake(self, other_party, shaking_side: HandshakeSide, obj_to_pass=None):
        if not isinstance(other_party, Handshake):
            raise TypeError(f'other_party must be type {Handshake}, not {type(other_party)}')
        if self._busy:
            self.log.debug(f'PROGRAMMING BUG: self._busy is still True, \
                           this should have been checked first by \
                           other party {other_party.name}')
            return False
        if self._shaking_with:
            raise Exception(f'PROGRAMMING BUG: handshake is NOT busy, \
                            but self._shaking_with is still set!!')
        if self._require_obj_to_pass and obj_to_pass is None:
            raise Exception(f'PROGRAMMING BUG: "self._require_obj_to_pass" is set \
                            but parameter "obj_to_pass" is None')
        if self._require_obj_to_pass and self._required_obj_type:
            if not isinstance(obj_to_pass, self._required_obj_type):
                raise TypeError(f'self._required_obj_type is {self._required_obj_type}, \
                                but provided obj_to_pass is type {type(obj_to_pass)}')
        if shaking_side == HandshakeSide.Give:
            if not self.ready_to_init_give():
                raise Exception(f'PROGRAMMING BUG: our side not ready to init give! \
                                This should have been checked by _initiate() before calling')
            else:
                self.log.debug(f'Beginning handshake as Giver with {other_party.name}')
                self._my_side = HandshakeSide.Give
                self._shaking_with = other_party
                self._pending = True
                self._ready = False
                self._transferring = False
                self._busy = True
                self._this_side_done = False
                self._obj_to_pass = obj_to_pass
                return True
        elif shaking_side == HandshakeSide.Receive:
            if not self.ready_to_init_receive():
                raise Exception('PROGRAMMING BUG: our side not ready to init receive! This should have been checked by _initiate() before calling')
            else:
                self.log.debug(f'Beginning handshake as Receiver with {other_party.name}')
                self._my_side = HandshakeSide.Receive
                self._shaking_with = other_party
                self._pending = True
                self._ready = False
                self._transferring = False
                self._busy = True
                self._this_side_done = False
                self._obj_to_pass = obj_to_pass
                return True
        else:
            raise ValueError(f'shaking_side ({shaking_side}) is not a valid HandshakeSide')


    def cancel_handshake(self, other_party):
        if not isinstance(other_party, Handshake):
            raise TypeError(f'other_party must be type {Handshake}, not {type(other_party)}')
        if not self._busy:
            self.log.warn('PROGRAMMING BUG: cancel_handshake: we are NOT busy, {other_party.name} should have checked first')
            return True
        if not self._shaking_with:
            raise Exception('PROGRAMMING BUG: cancel_handshake: we are busy, but self._shaking_with is not defined')
        if self._shaking_with is other_party or other_party is self:
            self._clear_handshake_vars()
            return True
        else:
            #should we potentially allow a different Handshaker to cancel us? probably not
            return False

    def _ABORT(self):
        '''DO NOT USE THIS IN PRACTICE, ONLY FOR DEV DEBUGGING
        '''
        self._clear_handshake_vars()
        self.__state = 0

    def _begin_transfer(self):
        '''only call from owner of this class instance
        '''
        if not self._ready_to_transfer:
            raise Exception('PROGRAMMING BUG: should have checked for self._ready_to_transfer before calling begin_transfer!')
        self.log.debug(f'{self._my_side.name}: Beginning transfer')
        self._ready_to_transfer = False
        self._transferring = True

    def _complete_transfer(self):
        '''only call from owner of this class instance
        '''
        if not self._transferring:
            raise Exception('PROGRAMMING BUG: this should not have been called if we were not already transferring')
        self.log.debug(f'{self._my_side.name}: Transfer complete')
        self._transferring = False
        self._this_side_done = True

    def __complete_handshake(self):
        '''This can only be called by the Giving side
        '''
        if not self._busy:
            raise Exception('We are not busy, there should be no handshake to complete!')
        if not self._shaking_with:
            raise Exception('PROGRAMMING BUG: We are busy, but do not know who we are shaking with (self._shaking_with is None)')
        if not self._my_side:
            raise Exception('PROGRAMMING BUG: We are busy, but do not know which side of the handshake we are (self._my_side is None)')
        if self._my_side != HandshakeSide.Give:
            raise Exception('PROGRAMMING BUG (probably): our side is not HandshakeSide.Give, we are not allowed to complete the handshake!')
        if not self.is_done_transferring():
            raise Exception('We are not done transferring! Cannot complete yet!')
        if not self._shaking_with.is_done_transferring():
            raise Exception(f'Other side ({self._shaking_with.name}) is not done transferring. Cannot complete yet!')
        self.log.debug(f'Handshake Complete with {self._shaking_with.name}')
        self._shaking_with._clear_handshake_vars()
        self._clear_handshake_vars()

    def main(self):

        self._SM.run()

        if self._SM.state == 0:
            #idle, waiting for a thing to begin by calling initiate_handshake
            if self._pending:
                self._SM.log_debug(f'Handshake initiated with {self._shaking_with.name}, with me as {self._my_side}')
                self._SM.state = 100

        elif self._SM.state == 100:
            if self._ready and self._shaking_with.is_ready():
                self._SM.log_debug('Both sides are ready')
                self._pending = False
                if self._my_side == HandshakeSide.Give:
                    self._SM.log_debug('Ready to transfer as Giver')
                    self._ready_to_transfer = True
                    self._SM.state = 200
                elif self._my_side == HandshakeSide.Receive:
                    self._ready_to_transfer = False #just to be sure this is False
                    self._SM.state = 400
                else:
                    raise Exception(f'self._my_side {self._my_side} is not a valid HandshakeSide')

        elif self._SM.state == 200:
            #AS GIVER: waiting for our own transferring to begin
            if self._transferring:
                self._SM.log_debug('Detected we are transferring as Giver')
                self._SM.state = 250

        elif self._SM.state == 250:
            #AS GIVER: waiting for our own transferring to complete
            if not self._transferring:
                self._SM.log_debug('Detected we are done transferring as Giver, now waiting for Receiver to start and complete transfer')
                self._SM.state = 300

        elif self._SM.state == 300:
            #AS GIVER: waiting for other side to be done
            if self._shaking_with.is_done_transferring():
                self._SM.log_debug('...other side is done transferring as Receiver, we should be all done now')
                self._SM.state = 600


        elif self._SM.state == 400:
            #AS RECEIVER: waiting for other side to begin transferring as the Giver
            if self._shaking_with.is_transferring():
                self._SM.log_debug('...other side start transferring as Giver')
                self._SM.state = 450
            elif self._shaking_with.is_done_transferring():
                #this means the transfer happened in a single scan and we missed it
                self._SM.state = 450

        elif self._SM.state == 450:
            #AS RECEIVER: waiting for other side to finish transferring as the Giver
            if self._shaking_with.is_done_transferring():
                self._SM.log_debug('...other side is done transferring as Giver')
                self._SM.log_debug('We are ready to transfer as Receiver')
                self._ready_to_transfer = True
                self._SM.state = 500

        elif self._SM.state == 500:
            #AS RECEIVER: waiting for our side to begin transferring
            if self._transferring:
                self._SM.log_debug('Detected we are transferring as Receiver')
                self._SM.state = 550

        elif self._SM.state == 550:
            #AS RECEIVER: waiting for our own transferring to complete
            if not self._transferring:
                self._SM.log_debug('Detected we are Done transferring as Receiver')
                self._SM.state = 600

        elif self._SM.state == 600:
            if self._my_side == HandshakeSide.Give:
                #the Receive side will sit in this state and wait for the Give side to complete the handshake
                self._SM.log_debug('Handshake Complete as Giver')
                self.__complete_handshake()
                self._SM.state = 0
            elif not self._busy:
                self._SM.log_debug('State 600: Handshake Complete as Receiver')
                self._SM.state = 0

        else:
            self._SM.state_undefined()

        self._SM.transition()

class TestGiver():
    def __init__(self, name: str='TestGiver'):
        self.name = name
        self.handshake = Handshake(name=self.name)


class TestReceiver():
    def __init__(self, name: str='TestReceiver'):
        self.name = name
        self.handshake = Handshake(name=self.name)

if __name__=='__main__':
    arm_logging_init('test_Handshake')
    log = logging.getLogger(__name__)
    log.setLevel(logging.DEBUG)
    shake1 = Handshake('shake1_Tx', log_level=logging.DEBUG)
    shake2 = Handshake('shake2_Rx', log_level=logging.DEBUG)

    killer = GracefulKiller()

    myKeyMapper = KeyboardMapper()
    myKeyMapper.add_mapping('1',
                            lambda : log.debug('Handshake successfully initiated!') if shake1._initiate(shake2, HandshakeSide.Give)
                            else log.error('Handshake init FAILED.'))
    myKeyMapper.add_mapping('2',
                            lambda : shake1.set_ready() if shake1.is_busy() and shake1.is_pending_ready()
                            else log.error('shake1 not Busy and Pending_Ready, cannot set ready flag'))
    myKeyMapper.add_mapping('3',
                            lambda:shake2.set_ready() if shake2.is_busy() and shake2.is_pending_ready()
                            else log.error('shake2 not Busy and Pending_Ready, cannot set ready flag'))
    myKeyMapper.add_mapping('4',
                            lambda:shake1._begin_transfer() if shake1.is_ready_to_transfer()
                            else log.error('shake1 not ready to transfer!'))
    myKeyMapper.add_mapping('5',
                            lambda:shake1._complete_transfer() if shake1.is_transferring()
                            else log.error('shake1 not transferring, cannot complete transfer'))
    myKeyMapper.add_mapping('6',
                            lambda:shake2._begin_transfer() if shake2.is_ready_to_transfer()
                            else log.error('shake2 not ready to transfer!'))
    myKeyMapper.add_mapping('7',
                            lambda:shake2._complete_transfer() if shake2.is_transferring()
                            else log.error('shake2 is not transferring, cannot complete transfer'))

    def abort_shakes():
        log.info('ABORTING shake1 and shake2')
        shake1._ABORT()
        shake2._ABORT()
    myKeyMapper.add_mapping('q', abort_shakes)

    def print_statuses():
        shake1.print_status()
        shake2.print_status()
    myKeyMapper.add_mapping('p', print_statuses)

    def print_states():
        print(f'{shake1.name} state: {shake1.get_state()}')
        print(f'{shake2.name} state: {shake2.get_state()}')
    myKeyMapper.add_mapping('s', print_states)

    def show_help():
        print()
        print('Handshake Tester help:')
        print('h:  help - show this dialog')
        print('q:  ABORT both Giver and Receiver to start over')
        print('s:  Print states of Giver and Receiver state machines')
        print('1:  Initiate handshake')
        print('2:  Call shake1.set_ready() (Giver)')
        print('3:  Call shake2.set_ready() (Receiver)')
        print('4:  Begin shake1 transfer (Giver)')
        print('5:  Complete shake1 transfer (Giver)')
        print('6:  Begin shake2 transfer (Receiver)')
        print('7:  Complete shake2 transfer (Receiver)')
    myKeyMapper.add_mapping('h', show_help)

    myKeyMapper.start()


    while not killer.kill_now:
        try:
            shake1.main()
            shake2.main()
            time.sleep(0.01)
        except Exception as ex:
            log.exception(ex)
            break

