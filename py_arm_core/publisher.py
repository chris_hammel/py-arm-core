import sys
import paho.mqtt.client as mqtt
import logging
import logging.config
import threading
import time
from numbers import Number
#try:
#    from .ARM_util import LIMIT
#except ImportError:
#    from ARM_util import LIMIT


class Publisher():
    '''MQTT Publishing client
    client_id must be unique
    '''
    def __init__(self, client_id='',
                        print_log=False,
                        broker_address='127.0.0.1',
                        broker_port=1883,
                        pub_callback=None,
                        default_topic='default',
                        name='MQTT_Publisher',
                        default_qos: int=1):
        try:
            self.log = logging.getLogger(f'{self.__class__.__name__}-{name}')
            self.log.setLevel(logging.WARN)
            self.name = name
            self.msg_count_in = 0
            self.msg_count_out = 0
            self._kill = False
            self.connected = False
            self._thread_count = 0
            self._default_qos = default_qos
            self.client = mqtt.Client(client_id=client_id)
            self.client.max_inflight_messages_set(50)
            if print_log:
                self.client.on_log = self.on_log
            self.client.on_disconnect = self.on_disconnect
            self.client.on_connect = self.on_connect
            self.client.on_publish = self.on_publish

            self._broker_address = broker_address
            self._broker_port = broker_port
            self._default_topic = default_topic
            self._prev_msg = None
            self._last_publish_time = None
            self.client.loop_start()
            self._init_connection()
        except Exception as ex:
            print(f"Publisher {name}: exception during init: {ex}")
            self.log.exception(ex)
            raise


    def increment_thread_count(self):
        if self._thread_count + 1 > 1024:
            self._thread_count = 0
        else:
            self._thread_count += 1

    def kill_threads(self):
        self._kill = True
        #self.client.loop_stop()
        self.client.disconnect()

    def _init_connection(self):
        while True and not self._kill:
            try:
                self.log.info('Connecting to broker...')
                self.client.connect(self._broker_address, self._broker_port, 5)
                break
            except Exception as ex:
                print(f"Error connection to broker (trying again in 2 sec): {ex}")
                self.log.exception(f"Error connection to broker (trying again in 2 sec): {ex}")
                time.sleep(2)

    def get_status(self):
        return {'name': self.name,
                'connected': self.connected,
                'msg_count_in': self.msg_count_in,
                'msg_count_out': self.msg_count_out}

    def on_publish(self, client, userdata, mid):
        self.log.info('data published \n')
        pass

    def on_log(self, client, userdata, level, buf):
        self.log.info("log: %s", str(buf))

    def on_connect(self, client, userdata, flags, rc):
        self.log.info('Connected')
        self.connected = True

    def on_disconnect(self, client, userdata, rc=0):
        self.connected = False
        self.log.info('Disconnected from broker')

    def publish(self, parent_topic=None, topic=None, msg='', qos:int=None):
        self.msg_count_in += 1
        if parent_topic is None:
            parent_topic = self._default_topic
        full_topic = ''
        if not topic is None:
            topic = topic.strip('/')
            full_topic = f'{parent_topic}/{topic}'
        if qos is None:
            qos = self._default_qos
        qos = 0 if qos < 0 else (2 if qos > 2 else qos)
        try:
            if self.connected:
                self.client.publish(full_topic, msg, qos, retain=False)
                self._prev_msg = msg
                self.msg_count_out += 1
            else:
                self.log.warn(f"Not connected to broker, cannot publish msg: {msg}")
        except Exception as ex:
            self.log.exception(ex)

    def publish_if_change(self, parent_topic=None, topic=None, msg='', qos=1):
        if msg != self._prev_msg:
            self.publish(parent_topic=parent_topic, topic=topic, msg=msg, qos=qos)

    def publish_on_interval(self, parent_topic=None, topic=None, msg='', qos=1, interval_sec: Number=1):
        pass


if __name__=='__main__':
    #logging.config.fileConfig('logging.conf')
    logging.basicConfig(level=logging.INFO, stream=sys.stdout)
    log = logging.getLogger(__name__)

    myPub = Publisher()
    while True:
        if myPub.connected:
            break
    myPub.publish(msg='TEST MSG', topic='test_topic')
    while True:
        try:
            time.sleep(1)
        except KeyboardInterrupt:
            break

    myPub.kill_threads()
