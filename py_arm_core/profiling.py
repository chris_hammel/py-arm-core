from functools import wraps
import time
#from snoop import snoop, pp
import inspect
from numbers import Number
import numpy as np

def timeit(func):
    '''Print the run time of the decorated function'''
    @wraps(func)
    def wrapper_timer(*args, **kwargs):
        if len(args) > 0:
            if inspect.isclass(type(args[0])):
                self = args[0]
                try:
                    getattr(self, 'name')
                    print_name = self.name + '.'
                except AttributeError:
                    print_name = self.__class__.__name__ + '.'
            else:
                print_name = ''
        else:
            print_name = ''
        start_time = time.perf_counter()
        value = func(*args, **kwargs)
        end_time = time.perf_counter()
        run_time = end_time - start_time
        print(f"Finished '{print_name}{func.__name__}' in {run_time:.6f} sec")
        return value
    return wrapper_timer

class ClassTiming():

    @classmethod
    def timeit(cls):
        '''must be called with () as a function when used as a decorator'''
        def deco(func):
            @wraps(func)
            def inner(*args, **kwargs):
                self = args[0]
                if getattr(self, 'name'):
                    print_name = self.name
                else:
                    print_name = self.__class__.__name__

                start_time = time.perf_counter()
                value = func(*args, **kwargs)
                end_time = time.perf_counter()
                run_time = end_time - start_time
                print(f"Finished {print_name}.{func.__name__!r} in {run_time:.6f} sec")
                return value
            return inner
        return deco

class timeit_accumulate(object):
    '''Accumulating timer\n
    Example:
        my_timer = timeit_accumulate()
        def my_func(args):
            //do stuff

        for _ in range(1000):
            my_timer.timeit(my_func)(args)

        my_timer.print_info()
    '''
    def __init__(self):
        self.total_run_time = 0.0
        self.run_count = 0
        self.avg_run_time = 0.0
        self.max_time = None
        self.min_time = None

    def timeit(self, func):
        @wraps(func)
        def wrapper_timer(*args, **kwargs):
            # if len(args) > 0:
            #     if inspect.isclass(type(args[0])):
            #         self = args[0]
            #         try:
            #             getattr(self, 'name')
            #             print_name = self.name + '.'
            #         except AttributeError:
            #             print_name = self.__class__.__name__ + '.'
            #     else:
            #         print_name = ''
            # else:
            #     print_name = ''
            start_time = time.perf_counter()
            value = func(*args, **kwargs)
            end_time = time.perf_counter()
            run_time = end_time - start_time
            # print(f"Finished '{func.__name__}' in {run_time:.6f} sec")
            self._add_run_time(run_time)
            #self.print_info()
            return value
        return wrapper_timer

    def print_info(self):
        print()
        print(f'{self.total_run_time=}')
        print(f'{self.run_count=}')
        print(f'{self.avg_run_time=}')
        print(f'{self.max_time=}')
        print(f'{self.min_time=}')
        print()

    def _add_run_time(self, new_time: Number):
        if self.max_time is None:
            self.max_time = new_time
        elif new_time > self.max_time:
            self.max_time = new_time

        if self.min_time is None:
            self.min_time = new_time
        elif new_time < self.min_time:
            self.min_time = new_time

        self.total_run_time += new_time
        self.run_count += 1
        self.avg_run_time = self.total_run_time / float(self.run_count)



class _timeit_accumulate(object):
    def __init__(self, new_instance):
        new_instance = 'self'
        self.total_run_time = 0.0
        self.run_count = 0
        self.avg_run_time = 0.0
        self.max_time = None
        self.min_time = None

    def __call__(self, func):
        @wraps(func)
        def wrapper_timer(*args, **kwargs):
            # if len(args) > 0:
            #     if inspect.isclass(type(args[0])):
            #         self = args[0]
            #         try:
            #             getattr(self, 'name')
            #             print_name = self.name + '.'
            #         except AttributeError:
            #             print_name = self.__class__.__name__ + '.'
            #     else:
            #         print_name = ''
            # else:
            #     print_name = ''
            start_time = time.perf_counter()
            value = func(*args, **kwargs)
            end_time = time.perf_counter()
            run_time = end_time - start_time
            print(f"Finished '{func.__name__}' in {run_time:.6f} sec")
            self._add_run_time(run_time)
            self.print_info()
            return value
        return wrapper_timer

    def print_info(self):
        print(f'{self.total_run_time=}')
        print(f'{self.run_count=}')
        print(f'{self.avg_run_time=}')
        print(f'{self.max_time=}')
        print(f'{self.min_time=}')

    def _add_run_time(self, new_time: Number):
        if self.max_time is None:
            self.max_time = new_time
        elif new_time > self.max_time:
            self.max_time = new_time

        if self.min_time is None:
            self.min_time = new_time
        elif new_time < self.min_time:
            self.min_time = new_time

        self.total_run_time += new_time
        self.run_count += 1
        self.avg_run_time = self.total_run_time / float(self.run_count)

if __name__=='__main__':
    def test_stuff():
        y = []
        for x in range(10000):
            y.append(x**2)

    my_timer = timeit_accumulate()

    for _ in range(100):
        my_timer.timeit(test_stuff)()

    my_timer.print_info()
