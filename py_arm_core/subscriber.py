#from elasticsearch import Elasticsearch
#from elasticsearch import helpers
import sys
import paho.mqtt.client as mqtt
import time
import logging
import threading
#from worker_threads import *
#from logger import log
import logging
import ast

class Subscriber():
    def __init__(self, client_id='',
                        subscribe_topic='#',
                        print_log=False,
                        broker_address='127.0.0.1',
                        broker_port=1883,
                        sub_callback=None,
                        name='MQTT_Subscriber'):
        self.log = logging.getLogger(self.__class__.__name__)
        self.name = name
        self.msg_count_in = 0
        self.msg_count_out = 0
        self._kill = False
        self.connected = False
        self._thread_count = 0
        self.client = mqtt.Client(client_id=client_id)
        self.client.on_message = self.on_message
        if print_log:
            self.client.on_log = self.on_log
        self.client.on_disconnect = self.on_disconnect
        self.client.on_connect = self.on_connect
        self._sub_callback = sub_callback
        try:
            self._sub_topic = ast.literal_eval(subscribe_topic)
        except Exception:
            self._sub_topic = subscribe_topic
            pass

        self._broker_address = broker_address
        self._broker_port = broker_port
        self.client.loop_start()
        self._init_connection()

    def kill_threads(self):
        self._kill = True
        self.client.loop_stop()
        self.client.disconnect()

    def increment_thread_count(self):
        if self._thread_count + 1 > 1024:
            self._thread_count = 0
        else:
            self._thread_count += 1

    def _init_connection(self):
        while True and not self._kill:
            try:
                self.log.info('Connecting to broker...')
                self.client.connect(self._broker_address, self._broker_port)
                break
            except Exception:
                pass
                self.log.exception("Error connecting to broker (trying again in 2 sec)")
                time.sleep(2)
                #if not self._kill:
                #    new_thread = threading.Timer(2, self._init_connection)
                #    new_thread.setName(name = f'{__class__.__name__}-init_connection-{self._thread_count}')
                #    self.increment_thread_count()
                #    new_thread.start()

    def get_status(self):
        return {'name': self.name,
                'connected': self.connected,
                'msg_count_in': self.msg_count_in,
                'msg_count_out': self.msg_count_out}

    def print_msg_count(self):
        self.log.debug("msg_count: %d,   internal cnt: %d  threads: %d", self.msg_count, self.client.msg_count, threading.active_count())

    def on_message(self, client, userdata, message: mqtt.MQTTMessage):
        self.msg_count_in += 1
        try:
            if self._sub_callback:
                self._sub_callback((message.payload, message._topic))
                self.msg_count_out += 1
        except Exception as ex:
            self.log.exception(ex)

    def on_log(self, client, userdata, level, buf):
        self.log.info("log: %s", str(buf))

    def on_connect(self, client, userdata, flags, rc):
        if rc == 0:
            self.log.info('Connected')
            self.connected = True
            if isinstance(self._sub_topic, list):
                [self.client.subscribe(curr_topic, qos=2) for curr_topic in self._sub_topic]
            else:
                self.client.subscribe(self._sub_topic, qos=2)
        else:
            self.connected = False
            self.log.error("Bad connection, return code=%d", rc)

    def on_disconnect(self, client, userdata, rc=0):
        self.connected = False
        self.log.info("Disconnected, result code: %d", rc)
        #logging.debug("Disconnect result code: " + str(rc))

if __name__ == '__main__':
    def test_callback(data):
        print(data)
    mySubscriber = Subscriber(broker_address='172.16.6.77', sub_callback=test_callback, print_log=True)
    time.sleep(2)
    mySubscriber.client.publish('test/sub_topic', 'test message')
    while True:
        time.sleep(.01)
    mySubscriber.client.loop_stop()
    mySubscriber.client.disconnect()
