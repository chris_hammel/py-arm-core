#ARM_util.py - Version 1.1
'''1.1 - added R_TRIG and F_TRIG
1.0 - initial pull from plc_msgs repo, contained only class TimedThread
'''
import threading
import sys
import logging
import logging.config
import time
try:
    from .Decorators import *
except ImportError:
    from Decorators import *
import keyboard
from typing import Callable
import signal
from numbers import Number
#from .arm_logging import *
try:
    from .arm_logging import *
except ImportError:
    from arm_logging import *



class TimedThread(threading.Thread):
    '''Creates a thread to run a function at a designated interval
    '''
    def __init__(self, name: str, func: Callable, interval_sec: Number=1, is_daemon: bool=True):
        self._func = func
        self.log = logging.getLogger(f'{self.__class__.__name__}-{name}')
        self._interval_sec = interval_sec
        self._name = name
        self._kill = False
        self.lock = threading.RLock()
        super().__init__(target=self.run_func, daemon=is_daemon)
        super().setName(name)


    def start(self):
        if self._func:
            try:
                super().start()
            except RuntimeWarning as ex:
                self.log.exception(f"Failed to start new thread: {ex}")
        else:
            self.log.error('No function provided, cannot start running on interval')

    @Class_Method_Decorators.lock_decorator(verbose=False)
    def kill(self):
        self.log.info("killing threads")
        self._kill = True

    def run_func(self):
        while not self._kill:
            self._locked_func()
            time.sleep(self._interval_sec)

    @Class_Method_Decorators.lock_decorator(verbose=False)
    def _locked_func(self):
        try:
            self._func()
        except Exception as ex:
            self.log.exception(ex)

class GracefulKiller():
    kill_now = False
    def __init__(self, callback_kill_func=None):
        self.log = logging.getLogger(self.__class__.__name__)
        self.log.setLevel(logging.INFO)
        signal.signal(signal.SIGINT, self.exit_gracefully)
        signal.signal(signal.SIGTERM, self.exit_gracefully)
        self._callback_kill_func = callback_kill_func

    def exit_gracefully(self, signum, frame):
        if not self.kill_now:
            self.log.info("exit_gracefully")
            self.kill_now = True
            if self._callback_kill_func:
                self._callback_kill_func()

class R_TRIG():
    '''boolean rising edge trigger - R_TRIG.Q goes high for one scan on a rising edge
    '''
    def __init__(self):
        self.__prev_input = False
        self.Q = False

    def run(self, new_input: bool) -> bool:
        if not self.__prev_input and new_input:
            self.Q = True
        else:
            self.Q = False

        self.__prev_input = new_input
        return self.Q


class F_TRIG():
    '''boolean falling edge trigger - F_TRIG.Q goes high for one scan on a falling edge
    '''
    def __init__(self):
        self.__prev_input = False
        self.Q = False

    def run(self, new_input: bool) -> bool:
        if self.__prev_input and not new_input:
            self.Q = True
        else:
            self.Q = False

        self.__prev_input = new_input
        return self.Q

class KeyboardMapper():
    '''Map key combinations to functions
    Add mappings using add_mapping()
    Call start() to begin the listener thread
    '''
    def __init__(self):
        self.__log = logging.getLogger(self.__class__.__name__)
        self.__log.setLevel(logging.INFO)
        self.mappings = {}
        self._main_thread = threading.Thread(name=f'{self.__class__.__name__}-main', target=self.check_for_keypress, daemon=True)
        self.__triggers = []

    def add_mapping(self, key: str, func: Callable, allow_key_hold: bool=False, on_keydown: bool=True, on_keyup: bool=False):
        if not key:
            raise Exception('key cannot be None')
        if not func:
            raise Exception('func cannot be None')
        if key in self.mappings.keys():
            self.__log.warn(f'key {key} already exists, is now being overwritten')

        if allow_key_hold:
            key_hold_func = lambda x:x
        else:
            key_hold_func = lambda x:False

        if on_keydown:
            new_r_trig = R_TRIG()
            self.__triggers.append(new_r_trig)
            on_keydown_func = new_r_trig.run
        else:
            on_keydown_func = lambda x:False

        if on_keyup:
            new_f_trig = F_TRIG()
            self.__triggers.append(new_f_trig)
            on_keyup_func = new_f_trig.run
        else:
            on_keyup_func = lambda x:False

        self.mappings[key] = [func, key_hold_func, on_keydown_func, on_keyup_func]

    def start(self):
        self._main_thread.start()

    def check_for_keypress(self):
        while True:
            try:
                for key in self.mappings.keys():
                    is_pressed = keyboard.is_pressed(key)
                    if self.mappings[key][1](is_pressed) or self.mappings[key][2](is_pressed) or self.mappings[key][3](is_pressed):
                        try:
                            self.mappings[key][0]()
                            break
                        except Exception as ex:
                            self.__log.exception(f'Exception for key {key} running function {self.mappings[key][0]}\n{ex}')
                time.sleep(0.010)
            except (KeyboardInterrupt, SystemExit):
                break

def LIMIT(value: Number, min_value: Number=None, max_value: Number=None):
    '''Bounds "value" to be in between min_value and max_value
    return value will always be the same type as "value"
    '''
    logger = logging.getLogger('LIMIT')
    #logger.setLevel(logging.DEBUG)
    if min_value is None and max_value is None:
        raise ValueError('Both "min_value" and "max_value" cannot be None')
    if value is None:
        raise ValueError('Input "value" cannot be None')
    if (not min_value is None) and (not max_value is None):
        if min_value > max_value:
            raise ValueError('Input "min_value" must be less than "max_value"')
    if (not min_value is None) and value < min_value:
        ret_val = min_value
    elif (not max_value is None) and value > max_value:
        ret_val = max_value
    else:
        ret_val = value
    if ret_val != value:
        pass
        #logger.debug(f'Input value {value} out of range, set to {ret_val}')
    return type(value)(ret_val)

class StateMachine():
    def __init__(self,
                 name: str='',
                 history_length: int=50,
                 post_transition_messages: bool=False,
                 user_log_level=logging.INFO):
        self.name = name+'-SM'
        self.post_transition_messages = post_transition_messages
        self.__log = arm_logger(self.name+'-internal',
                                log_level=logging.INFO,
                                parent_name='')
        # self.__log = logging.getLogger(f'{self.__class__.__name__}-{name}')
        # self.__log.setLevel(logging.INFO)
        self._user_logger = arm_logger(self.name,
                                       log_level=user_log_level)#,
                                       #parent_name=name)#logging.getLogger(name)
        #self._user_logger.setLevel(user_log_level)

        #triggers
        self._enabled_trig = R_TRIG()
        self._entering_state_trig = R_TRIG()
        self._exiting_state_trig = R_TRIG()

        self.__heartbeat = 0
        self.__state_start_of_scan = 0
        self.state = 0
        self.__prev_state = 0
        self._state_history = [None]*LIMIT(history_length, min_value=10, max_value=500)
        self.__entering_state = True
        self.__exiting_state = False
        self.__scans_in_state = 0
        self.__enabled = False
        self.__busy = False
        self.__first_scan = True
        self.timer = TON()
        self.timer2 = TON()
        self.timer_expired = False
        self.timer2_expired = False
        self.enable = False
        self.flag = False
        self.state_desc = ''
        self.swsource = f'{self.name} (State {self.state})'

    def mqtt_connected(self):
        '''returns True only if ALL defined mqtt connections are live'''
        if self.__log.mqtt_enabled and not self.__log.mqtt_connected():
            return False
        if self._user_logger.mqtt_enabled and not self._user_logger.mqtt_connected():
            return False
        else:
            return True

    def mqtt_disconnected(self):
        '''returns True only if ALL defined mqtt connections are dead'''
        if not self.__log.mqtt_disconnected():
            return False
        if not self._user_logger.mqtt_disconnected():
            return False
        else:
            return True

    def kill_threads(self):
        if self.__log.mqtt_connected():
            self.__log.kill_threads()
        if self._user_logger.mqtt_connected():
            self._user_logger.kill_threads()

    #PROPERTIES SET ONLY INTERNALLY
    @property
    def heartbeat(self):
        return self._heartbeat

    @heartbeat.setter
    def __heartbeat(self, value: int):
        if value is None:
            raise Exception('value cannot be None')
        else:
            self._heartbeat = value

    @property
    def prev_state(self):
        return self._prev_state

    @prev_state.setter
    def __prev_state(self, value: int):
        if value is None:
            self.__log.error(f'prev_state.setter - value cannot be None')
        else:
            self._prev_state = LIMIT(value, min_value=0)

    @property
    def state_start_of_scan(self):
        return self._state_start_of_scan

    @state_start_of_scan.setter
    def __state_start_of_scan(self, value: int):
        self._state_start_of_scan = value

    @property
    def entering_state(self):
        return self._entering_state

    @entering_state.setter
    def __entering_state(self, value: bool):
        if value is None:
            self.__log.error('value cannot be None')
        else:
            self._entering_state = value

    @property
    def exiting_state(self):
        return self._exiting_state

    @exiting_state.setter
    def __exiting_state(self, value: bool):
        '''un-tested'''
        if value is None:
            self.__log.error('value cannot be None')
        else:
            self._exiting_state = value

    @property
    def scans_in_state(self):
        return self._scans_in_state

    @scans_in_state.setter
    def __scans_in_state(self, value: int):
        if value is None:
            raise ValueError("value cannot be None")
        else:
            self._scans_in_state = value

    @property
    def enabled(self):
        return self._enabled

    @enabled.setter
    def __enabled(self, value: bool):
        if value is None:
            self._enabled = False
        else:
            self._enabled = value

    @property
    def busy(self):
        return self._busy

    @busy.setter
    def __busy(self, value: bool):
        if value is None:
            self._busy = False
        else:
            self._busy = value

    #PROPERTIES SET BY ANYONE
    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, value: str):
        self._name = value

    @property
    def state(self):
        return self._state

    @state.setter
    def state(self, value: int):
        if value is None:
            self.__log.error(f'state.setter - value cannot be None')
        else:
            try: self._state
            except AttributeError: self._state = value
            value = LIMIT(value, min_value=0)
            if value != self._state:
                self.__exiting_state = True
            self._state = value
            self.__exiting_state = self._exiting_state_trig.run(self._state != self.state_start_of_scan)

    @property
    def enable(self):
        return self._enable

    @enable.setter
    def enable(self, value: bool):
        if value is None:
            self._enable = False
        else:
            self._enable = value

    @property
    def flag(self):
        return self._flag

    @flag.setter
    def flag(self, value: bool):
        if value is None:
            self._flag = False
        else:
            self._flag = value

    def set_flag(self):
        self.flag = True

    def clear_flag(self):
        self.flag = False

    @property
    def state_desc(self):
        return self._state_desc

    @state_desc.setter
    def state_desc(self, value: str):
        if value is None:
            self._state_desc = ''
        else:
            self._state_desc = value

    @property
    def swsource(self):
        return self._swsource

    @swsource.setter
    def swsource(self, value: str):
        if value is None:
            self._swsource = ''
        else:
            self._swsource = value

    #METHODS
    def log(self, obj, log_level=logging.INFO):
        if log_level is None:
            log_level = logging.INFO
        self._user_logger.log(obj=f'{obj}', level=log_level, source=self.swsource)#msg=f'{self.swsource}: {obj}')

    def log_trace(self, obj):
        self.log(obj=obj, log_level='TRACE')

    def log_debug(self, obj):
        self.log(obj=obj, log_level=logging.DEBUG)

    def log_detail(self, obj):
        self.log(obj=obj, log_level='DETAIL')

    def log_info(self, obj):
        self.log(obj=obj, log_level=logging.INFO)

    def log_warn(self, obj):
        self.log(obj=obj, log_level=logging.WARN)

    def log_error(self, obj):
        self.log(obj=obj, log_level=logging.ERROR)

    def log_exception(self, obj):
        self._user_logger.exception(obj)
        #self.log(obj=obj, log_level=logging.EXCEPTION)

    def log_critical(self, obj):
        self.log(obj=obj, log_level=logging.CRITICAL)

    def state_undefined(self):
        raise Exception(f'State {self.state} is undefined!')

    def set_timer(self, duration_sec: Number):
        if duration_sec <= 0:
            raise ValueError(f'duration_sec must be <= 0: {duration_sec=}')
        #self.timer.run(IN=False, PT=duration_sec)
        self.timer._IN = True
        self.timer._PT = duration_sec

    def set_timer2(self, duration_sec):
        if duration_sec <= 0:
            raise ValueError(f'duration_sec must be <= 0: {duration_sec=}')
        self.timer2._IN = True,
        self.timer2._PT = duration_sec


    def run(self, sim_time=None):
        '''Call this before each scan
        if sim_time is specified, we will use sim_time for internal state timers instead of time.time() (meant for running a simulation)
        '''
        self.__heartbeat = self.heartbeat + 1
        if self.heartbeat >= 255:
            self.__heartbeat = 0
        self.__enabled = self._enabled_trig.run(self.enable)    #True for one scan on a rising edge
        self.__busy = self.state != 0
        self.swsource = f'(State {self.state})'
        self.__state_start_of_scan = self.state
        self.__exiting_state = self._exiting_state_trig.run(False)  #this will reset both __exiting_state and its trigger
        self.__entering_state = (self.state != self.prev_state) or self.__first_scan
        if self.entering_state:
            self._state_history.insert(0, {'state': self.prev_state, 'scans': self.scans_in_state})
            self.__scans_in_state = 1
            self.timer.run(IN=False, PT = 0)
            self.timer2.run(IN=False, PT = 0)
        else:
            self.__scans_in_state = self.scans_in_state + 1
            if sim_time is not None:
                self.timer.run(IN = self.timer._IN, PT = self.timer._PT, current_time=sim_time)
                self.timer2.run(IN = self.timer2._IN, PT=self.timer2._PT, current_time=sim_time)
            else:
                self.timer.run(IN = self.timer._IN, PT = self.timer._PT)
                self.timer2.run(IN = self.timer2._IN, PT = self.timer2._PT)

        self.timer_expired = self.timer.Q
        self.timer2_expired = self.timer2.Q

        self.__first_scan = False
        self.__prev_state = self.state

    def transition(self):
        '''Call this after each scan
        '''
        if self.state != self.prev_state:
            self.clear_flag()

            if self.post_transition_messages:
                self.log_trace(f'({self.state_desc}): Transitioning to State {self.state} after {self.scans_in_state} scans')
        self.__exiting_state = False
        if self.state != 0:
            self.__busy = True

class TON():
    def __init__(self, name: str='TON'):
        self.name = name
        self.Time = None
        self._Enable = None
        self._trig = R_TRIG()
        self.Q = False              #"output" - value is On or Off
        self.ET = 0.0               #"elapsed time" - 0 at start, ramps up to PT and stays there after Q goes on
        self._PT = None              #"pass time" - time to pass before output comes on - stored on exiting state 0
        self._state = 0
        self._first_scan = True
        self._start_time = None
        self._time = None
        self._target_duration = None
        self._is_simulation_time = False

    def is_sim_time(self):
        return self._is_simulation_time

    def _case_0(self):
        if self._trig.Q:
            self._start_time = self._time
            self._target_duration = self._target_duration
            self._state = 100

    def _case_100(self):
        if not self._IN:
            self.Q = False
            self.ET = 0.0
            self._state = 0
        else:
            self.ET = LIMIT((self._time - self._start_time), max_value = self._PT)
            if self._time - self._start_time >= self._PT:
                self.Q = True

    def _main_switch(self, state):
        CASE_MAPPING = {
            0: self._case_0,
            100: self._case_100}
        return CASE_MAPPING.get(state, None)


    def run(self, IN: bool, PT: float, current_time=None):
        if current_time is None:
            self._is_simulation_time = False
            self._time = time.time()
        else:
            self._is_simulation_time = True
            self._time = current_time
        self._PT = PT
        self._IN = IN
        self._trig.run(self._IN)

        self._main_switch(self._state)()

        return self.Q

def test_TON():
    arm_logging_set_basic_config()
    log = logging.getLogger('ARM_util.test_TON')
    myTimer = TON('MyTimer')
    #mySM = StateMachine('tester', post_transition_messages=True)
    prev_time = 0
    while True:
        try:
            myTimer.run(IN=not myTimer.Q, PT=2)
            if myTimer.Q:
                my_time = time.time()
                print(f'Q ON - time: {my_time - prev_time} interval')
                prev_time = my_time
        except KeyboardInterrupt:
            break
        except Exception as ex:
            log.exception(ex)
            break


if __name__ == '__main__':

    a = 1
    assign_if_greater(a, 2)
    print(a)
    # logging.basicConfig(level=logging.INFO, stream=sys.stdout)
    # arm_logging_init('ARM_util')

    # log = logging.getLogger('ARM_util')

    # myTimer = TON()
    # mySM = StateMachine('tester', post_transition_messages=True)
    # sim_time = 0.0
    # dt = 0.1
    # while True:
    #     sim_time += dt
    #     try:

    #         mySM.run(sim_time)
    #         if mySM.state == 0:
    #             if mySM.entering_state:
    #                 start_time = sim_time
    #                 mySM.set_timer(2)
    #                 #mySM.timer.run(IN=True, PT=2)
    #             if mySM.timer_expired:
    #                 print(f'Leaving state 0 after time {sim_time - start_time} sec')
    #                 mySM.state = 100

    #         elif mySM.state == 100:
    #             if mySM.entering_state:
    #                 mySM.state_desc='my first state'
    #                 mySM.log('Entering state 100!')
    #                 start_time = sim_time
    #                 mySM.set_timer(0.8)
    #                 #mySM.timer.run(IN=True, PT=5)
    #             if mySM.timer.Q:
    #                 mySM.state = 200
    #             if mySM.exiting_state:
    #                 mySM.log(f'Exiting state 100! after {sim_time - start_time} sec')

    #         elif mySM.state == 200:
    #             if mySM.entering_state:
    #                 start_time = sim_time
    #                 mySM.state_desc = 'my SECOND state'
    #             if mySM.timer.run(IN=True, PT=3, current_time=sim_time):
    #                 print(f'Leaving state 200 after {time.time() - start_time} sec')
    #                 mySM.state = 0

    #         else:
    #             mySM.state_undefined()

    #         mySM.transition()

    #         # my_time = time.time()
    #         # myTimer.run(IN=not myTimer.Q, PT=2)
    #         # if myTimer.Q:
    #         #     print(f'Q ON - time: {time.time() - prev_time} interval')

    #         # prev_time = my_time


    #     except KeyboardInterrupt:
    #         break
    #     except Exception as ex:
    #         log.exception(ex)
    #         break
