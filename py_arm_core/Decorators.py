#Decorators.py - Version 1.0
import threading
import functools
#from logger import log
import logging
import pickle
import os

class Class_Method_Decorators():

    @classmethod
    def lock_decorator(cls, verbose=False, timeout=10, lock_name='lock', blocking=True):
        """
        Acquire and release class Lock or RLock

        In a class, assign self.lock in __init__, either as
        threading.Lock() or threading.RLock()
        """
        def deco(func):
            def inner(*args, **kwargs):
                self = args[0]
                if (lock := getattr(self, lock_name)):
                    if verbose:
                        self.log.debug("{} trying to acquire lock...".format(func.__name__))
                    if not lock.acquire(blocking=blocking, timeout=timeout):
                        #if verbose:
                        self.log.error("{} FAILED to acquire lock.\t\t\t\t\tFAIL".format(func.__name__))
                        return None
                    if verbose:
                        self.log.debug("{} SUCCESSFULLY acquired lock.".format(func.__name__))
                    out = None
                    try:
                        out = func(*args, **kwargs)
                    except Exception as ex:
                        if self.log:
                            self.log.exception("Error in lock_decorator - {}: {}".format(func.__name__, ex))
                    finally:
                        lock.release()
                        if verbose:
                            self.log.debug("{} RELEASED lock.".format(func.__name__))
                        return out
            return inner
        return deco# (func)


class SelfSaving():
    def __init__(self, name: str):
        self.name = name
        self.log = logging.getLogger(self.__class__.__name__)

    def _check_for_logger(self):
        try: self.log
        except NameError:
            self.log = logging.getLogger(self.__class__.__name__)

    def save(self, directory: str=None) -> bool:
        self._check_for_logger()
        try:
            filename = self.name+'.p'
            if directory is not None:
                os.makedirs(directory, exist_ok=True)
                filename = os.path.join(directory, filename)
                #filename = '/'.join([directory, filename]).strip('/')
            self.log.info(f'pickle-dumping instance {self.name} of {self.__class__.__name__} to {filename}')
            pickle.dump(self.__dict__, open(filename, 'wb'))
            self.log.info(f'pickle-dump complete.')
            return True
        except Exception as ex:
            self.log.error(f'Failed to dump class instance {self.name} to file {filename}: {str(ex)}')
            return False

    def load(self, directory: str=None) -> bool:
        self._check_for_logger()
        try:
            filename = self.name+'.p'
            if directory is not None:
                filename = os.path.join(directory, filename)
                #filename = '/'.join([directory, filename]).strip('/')
            self.log.info(f'pickle-loading instance {self.name} of {self.__class__.__name__} from {filename}')
            self.__dict__ = pickle.load(open(filename, 'rb'))
            self.log.info(f'class loaded.')
            return True
        except Exception as ex:
            self.log.error(f'Failed to load class from file {filename}: {str(ex)}')
            return False

    def saved_instance_exists(self, directory: str=None) -> bool:
        filename = self.name+'.p'
        if directory is not None:
            filename = os.path.join(directory, filename)
        try:
            file = open(filename, 'r')
            file.close()
        except FileNotFoundError:
            return False
        return True

if __name__=='__main__':
    class test_class():
        def __init__(self):
            self.dummy = ''
            self.lock = threading.RLock()

        #@Class_Method_Decorators.lock_verbose # .lock_decorator(verbose=False)
        @Class_Method_Decorators.lock_decorator(verbose=True)
        def reset_dummy(self):
            self.dummy = ''

        def append_dummy(self, new_text):
            self.dummy += new_text

        @Class_Method_Decorators.lock_decorator(verbose=True)
        def print_dummy(self):
            print("Dummy: ", self.dummy)

        @Class_Method_Decorators.lock_decorator(True)
        def get_dummy(self, prefix):
            return prefix + self.dummy

    #myDR = DataReceiver()
    # cond_track = ConditionTracker()
    # print(cond_track.GetActiveConditions())
    MyTest = test_class()
    MyTest.append_dummy('dummy text')
    print(MyTest.get_dummy(prefix='my_prefix'))
