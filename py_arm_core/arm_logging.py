import logging
import paho.mqtt.client as mqtt
try:
    from .publisher import *
except ImportError:
    from publisher import *
import time
import sys
from datetime import datetime
import json
import tqdm

DETAIL_LEVEL_NUM = 15
TRACE_LEVEL_NUM = 5
SYSTEM_LEVEL_NUM = 55

FORMAT_STRING = "%(asctime)s:%(msecs)010.6f [%(levelname)-6s] %(name)-12s  - %(message)s"

class TqdmLoggingHandler(logging.Handler):
    def __init__(self, level=logging.NOTSET):
        super().__init__(level)
        self.format

    def emit(self, record):
        try:
            msg = self.format(record)
            tqdm.tqdm.write(msg)
            self.flush()
        except (KeyboardInterrupt, SystemExit):
            raise
        except:
            self.handleError(record)

class arm_logger():
    '''log_level will set the level for the internal python logging.Logger
    ALL levels will be sent out over mqtt, if it is enabled
    '''
    def __init__(self,
                 name: str,
                 parent_name: str='',
                 log_level = logging.INFO,
                 enable_mqtt: bool=None,
                 broker_address='127.0.0.1',
                 broker_port=1883,
                 wait_for_mqtt_connect: bool=False,
                 default_qos=1,
                 tqdm_writer: bool=True):

        global arm_logging_initialized
        try: arm_logging_initialized
        except NameError: arm_logging_initialized = None
        if not arm_logging_initialized:
            raise Exception('arm logging not initialized, must call arm_logging_init() before creating arm_loggers')

        global arm_logging_enable_tqdm

        self.name = name
        self.parent_name = parent_name
        self._default_source = '.'.join([parent_name, name]).strip('.')
        self._py_logger = logging.getLogger(name)
        self._py_logger.setLevel(log_level)

        if (arm_logging_enable_tqdm is None and tqdm_writer) \
           or (arm_logging_enable_tqdm is not None and arm_logging_enable_tqdm):
            formatter = logging.Formatter(fmt=FORMAT_STRING)
            new_handler = TqdmLoggingHandler()
            new_handler.setFormatter(formatter)
            self._py_logger.addHandler(new_handler)
            global arm_logging_to_file
            try: arm_logging_to_file
            except NameError: arm_logging_to_file = False
            if not arm_logging_to_file:
                self._py_logger.propagate = False

        # if tqdm_writer:
        #     def emit(self, record):
        #         try:
        #             msg = self.format(record)
        #             tqdm.tqdm.write(msg)
        #             self.flush()
        #         except (KeyboardInterrupt, SystemExit):
        #             raise
        #         except:
        #             self.handleError(record)


        #     setattr(self._py_logger, 'emit', emit)

        if enable_mqtt is None:
            global arm_logging_enable_mqtt
            enable_mqtt = arm_logging_enable_mqtt
        self.mqtt_enabled = enable_mqtt

        if enable_mqtt:
            global arm_logging_app_name
            self._pub = Publisher(name=name, default_topic=arm_logging_app_name, default_qos=0)
        else:
            self._pub = None

        if enable_mqtt and wait_for_mqtt_connect:
            if not self._pub.connected:
                self._py_logger.info('Waiting for publisher to connect...')
            while not self._pub.connected:
                if self._pub.connected:
                    break
                else:
                    time.sleep(0.1)
            self._py_logger.debug('Publisher connected')

    def kill_threads(self):
        if self._pub is not None:
            self._pub.kill_threads()

    def mqtt_connected(self):
        if self._pub is None:
            return False
        elif not self._pub.connected:
            return False
        else:
            return True

    def mqtt_disconnected(self):
        return not self.mqtt_connected()

    def log(self, obj, level, source: str=None):
        '''defaults to level INFO if log level is invalid
        '''
        if obj is None:
            raise Exception('obj to log cannot be None')
        if level is None:
            raise Exception('level cannot be None')

        if source is None:
            source = self._default_source
        else:
            source = '.'.join([self._default_source, source])
        if isinstance(level, str):
            try:
                level = logging.getLevelName(level)
            except Exception as ex:
                print(ex)
                return

        def get_level_text(level_num):
            switcher = {
                TRACE_LEVEL_NUM: 'TRACE',
                logging.DEBUG: 'DEBUG',
                DETAIL_LEVEL_NUM: 'DETAIL',
                logging.INFO: 'INFO',
                logging.WARN: 'WARN',
                logging.WARNING: 'WARN',
                logging.ERROR: 'ERROR',
                logging.CRITICAL: 'SYSTEM',
                SYSTEM_LEVEL_NUM: 'SYSTEM'
            }
            return switcher.get(level, logging.INFO)

        if self._pub:
            log_msg = {'LogData':
                        {'Msg': f'{obj}',
                         'SWSource': source,
                         'Severity': get_level_text(level)},
                       'EventTime': datetime.strftime(datetime.utcnow(), '%Y-%m-%dT%H:%M:%S.%fZ')
                      }
            self._pub.publish(topic='Machine_Msg', msg=json.dumps(log_msg), qos=0)

        self._py_logger.log(level, obj)

    def trace(self, obj, source: str=None):
        self.log(obj=obj, level=TRACE_LEVEL_NUM, source=source)

    def debug(self, obj, source: str=None):
        self.log(obj=obj, level=logging.DEBUG, source=source)

    def detail(self, obj, source: str=None):
        self.log(obj=obj, level=DETAIL_LEVEL_NUM, source=source)

    def info(self, obj, source: str=None):
        self.log(obj=obj, level=logging.INFO, source=source)

    def warn(self, obj, source: str=None):
        self.log(obj=obj, level=logging.WARN, source=source)

    def error(self, obj, source: str=None):
        self.log(obj=obj, level=logging.ERROR, source=source)

    def critical(self, obj, source: str=None):
        self.log(obj=obj, level=logging.CRITICAL, source=source)

    def system(self, obj, source: str=None):
        self.log(obj=obj, level=SYSTEM_LEVEL_NUM, source=source)

    def exception(self, obj, source: str=None):
        self._py_logger.exception(obj)

def arm_logging_set_basic_config(stream = sys.stdout, format_string: str=FORMAT_STRING, filename: str=None):
    fmtString = format_string
    if filename is not None:
        logging.basicConfig(format=fmtString, datefmt='%H:%M:%S', filename=filename)
        global arm_logging_to_file
        arm_logging_to_file = True
    else:
        logging.basicConfig(stream=stream, format=fmtString, datefmt='%H:%M:%S')

def arm_logging_init(application_name: str, enable_mqtt: bool=False, enable_tqdm: bool=None):

    #def new_log_level(number: int, name: str):
    #    logging.addLevelName(number, name)
    #    def trace(self, message, *args, **kwargs):
    #        if self.isEnabledFor(number):
    #            self._log(TRACE_LEVEL_NUM, message, args, **kwargs)
    #    logging.Logger.trace = trace
    #    logging.TRACE = TRACE_LEVEL_NUM


    global arm_logging_app_name
    arm_logging_app_name = application_name

    global arm_logging_enable_mqtt
    arm_logging_enable_mqtt = enable_mqtt

    #If enable_tqdm is None, then only the inputs to arm_logger.__init__ will determine whether or not it has a TqdmLoggingHandler
    global arm_logging_enable_tqdm
    arm_logging_enable_tqdm = enable_tqdm

    logging.addLevelName(SYSTEM_LEVEL_NUM, 'SYSTEM')
    def system(self, message, *args, **kwargs):
        if self.isEnabledFor(SYSTEM_LEVEL_NUM):
            self._log(SYSTEM_LEVEL_NUM, message, args, **kwargs)
    logging.Logger.system = system
    logging.SYSTEM = SYSTEM_LEVEL_NUM

    logging.addLevelName(DETAIL_LEVEL_NUM, 'DETAIL')
    def detail(self, message, *args, **kwargs):
        if self.isEnabledFor(DETAIL_LEVEL_NUM):
            self._log(DETAIL_LEVEL_NUM, message, args, **kwargs)
    logging.Logger.detail = detail
    logging.DETAIL = DETAIL_LEVEL_NUM

    logging.addLevelName(TRACE_LEVEL_NUM, 'TRACE')
    def trace(self, message, *args, **kwargs):
        if self.isEnabledFor(TRACE_LEVEL_NUM):
            self._log(TRACE_LEVEL_NUM, message, args, **kwargs)
    logging.Logger.trace = trace
    logging.TRACE = TRACE_LEVEL_NUM

    global arm_logging_initialized
    arm_logging_initialized = True

if __name__=='__main__':
    #logging.basicConfig(stream=sys.stdout)
    arm_logging_init('testing', enable_tqdm=True)
    arm_logging_set_basic_config(filename='testLog.txt')
    log = arm_logger('test', tqdm_writer=False)

    # for i in tqdm.tqdm(range(100)):
    #     log.info(f'testing - {i}')
    #     time.sleep(0.1)

    bar = tqdm.tqdm(total=100, desc='bar1')
    bar2 = tqdm.tqdm(total=100, desc='bar2')

    for i in range(100):
        bar2.update()
        log.info(f'test - {i}')
        if i%2 == 0:
            bar.update(1)
        else:
            bar.n -= 1
            bar.update(0)
        time.sleep(0.1)
    bar.close()
    bar2.close()
